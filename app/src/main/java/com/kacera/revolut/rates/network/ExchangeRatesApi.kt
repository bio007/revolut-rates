package com.kacera.revolut.rates.network

import com.kacera.revolut.rates.network.data.ExchangeRates
import retrofit2.http.GET
import retrofit2.http.Query

interface ExchangeRatesApi {
    companion object {
        const val BASE_URL = "https://revolut.duckdns.org"
    }

    @GET("latest")
    suspend fun getRates(
        @Query("base") base: String? = null,
        @Query("symbols") symbols: String? = null
    ): ExchangeRates
}