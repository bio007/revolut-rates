package com.kacera.revolut.rates.network.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ExchangeRates(
    @Json(name="base") val baseCurrency: String,
    @Json(name="rates") val exchangeRates: List<ExchangeRate>)