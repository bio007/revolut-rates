package com.kacera.revolut.rates.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

inline fun <reified T> LiveData<T>.asMutable(): MutableLiveData<T> {
    return if (this is MutableLiveData<T>) this else throw IllegalArgumentException("$this is not an instance of MutableLiveData!")
}