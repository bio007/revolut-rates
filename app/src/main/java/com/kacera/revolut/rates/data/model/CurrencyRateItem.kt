package com.kacera.revolut.rates.data.model

data class CurrencyRateItem(val currency: String, var amount: Double = 1.0)