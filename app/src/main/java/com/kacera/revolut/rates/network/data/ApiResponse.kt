package com.kacera.revolut.rates.network.data

sealed class ApiResponse<T> {
    class Success<T>(val result: T) : ApiResponse<T>()
    class Error<T>(val error: Throwable) : ApiResponse<T>()
}