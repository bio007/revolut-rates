package com.kacera.revolut.rates.viewmodel

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import com.kacera.revolut.rates.data.RatesAdapter
import com.kacera.revolut.rates.data.RatesRepository
import com.kacera.revolut.rates.data.model.CurrencyRateItem
import com.kacera.revolut.rates.network.data.ApiResponse
import com.kacera.revolut.rates.timer.ReloadTimer
import com.kacera.revolut.rates.utils.TextChangedTextWatcher
import com.kacera.revolut.rates.utils.asMutable
import com.kacera.revolut.rates.utils.parseSafe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.NumberFormat
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList

class RatesFragmentViewModel(
    private val ratesRepository: RatesRepository,
    private val timer: ReloadTimer,
    private val formatter: NumberFormat
) : ViewModel(), DefaultLifecycleObserver {

    val adapter: RatesAdapter
    val scrollPosition: LiveData<Int> = MutableLiveData<Int>()
    val apiError: LiveData<Boolean> =
        Transformations.distinctUntilChanged(MutableLiveData<Boolean>(false))

    private val currencyItems: MutableList<CurrencyRateItem> = CopyOnWriteArrayList()
    private val exchangeRates: MutableMap<String, Double> = ConcurrentHashMap()
    private var baseCurrency = "EUR"

    init {
        adapter = RatesAdapter(
            formatter,
            { item -> onCurrencySelected(item) },
            TextChangedTextWatcher { text -> onInputAmount(text.ifEmpty { "0" }) }
        )
    }

    override fun onCreate(owner: LifecycleOwner) {
        timer.reload.observe(owner) {
            viewModelScope.launch {

                val apiRates = when (val response = ratesRepository.getRates(baseCurrency)) {
                    is ApiResponse.Success -> {
                        apiError.asMutable().value = false
                        response.result
                    }
                    is ApiResponse.Error -> {
                        apiError.asMutable().value = true
                        return@launch
                    }
                }

                launch(Dispatchers.Default) {
                    exchangeRates[apiRates.baseCurrency] = 1.0
                    apiRates.exchangeRates.associateByTo(exchangeRates,
                        {rate -> rate.currencyName},
                        {rate -> rate.conversionRate})

                    if (currencyItems.isEmpty()) {
                        currencyItems.add(CurrencyRateItem(apiRates.baseCurrency))
                        currencyItems.addAll(apiRates.exchangeRates.map { entry ->
                            CurrencyRateItem(entry.currencyName, entry.conversionRate)
                        })
                    } else {
                        val baseAmount = currencyItems.first { it.currency == apiRates.baseCurrency }.amount
                        currencyItems.forEach {
                            it.amount = baseAmount * exchangeRates.getOrDefault(it.currency, 0.0)
                        }
                    }
                }.join()

                adapter.setItems(currencyItems)
            }
        }
    }

    @VisibleForTesting
    fun onInputAmount(amountText: String) {
        val amountInput = formatter.parseSafe(amountText).toDouble()
        currencyItems.forEach {
            it.amount =
                amountInput * exchangeRates.getOrDefault(
                    it.currency,
                    0.0
                )
        }

        adapter.setItems(currencyItems)
    }

    @VisibleForTesting
    fun onCurrencySelected(item: CurrencyRateItem) {
        currencyItems.apply {
            remove(item)
            add(0, item)
        }.also {
            adapter.setItems(it)
        }

        baseCurrency = item.currency
        exchangeRates[baseCurrency]?.let { newBase ->
            exchangeRates.replaceAll { _, rate -> rate * 1 / newBase }
        }

        scrollPosition.asMutable().value = 0
    }
}