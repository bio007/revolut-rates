package com.kacera.revolut.rates.utils

import com.kacera.revolut.rates.network.data.ExchangeRate
import com.squareup.moshi.*

class ExchangeRatesJsonAdapter {

    @FromJson
    fun fromJson(jsonReader: JsonReader): List<ExchangeRate> {
        val names = mutableListOf<String>()
        val rates = mutableListOf<Double>()

        jsonReader.beginObject()

        while (jsonReader.hasNext()) {
            when (jsonReader.peek()) {
                JsonReader.Token.NAME -> names.add(jsonReader.nextName())
                JsonReader.Token.NUMBER -> rates.add(jsonReader.nextDouble())
                else -> jsonReader.skipValue()
            }
        }

        jsonReader.endObject()

        if (names.size != rates.size) {
            throw JsonDataException("Found inconsistency in exchange rates data. Found ${names.size} currencies but ${rates.size} exchange rates")
        }

        return names.zip(rates) { name, rate -> ExchangeRate(name, rate) }
    }

    @ToJson
    fun toJson(writer: JsonWriter, rates: List<ExchangeRate>) {
        writer.beginObject()
        rates.forEach {
            writer.name(it.currencyName)
            writer.value(it.conversionRate)
        }
        writer.endObject()
    }
}