package com.kacera.revolut.rates.data

import com.kacera.revolut.rates.network.data.ApiResponse
import com.kacera.revolut.rates.network.data.ExchangeRates

interface RatesRepository {
    suspend fun getRates(base: String? = "EUR", symbols: String? = null): ApiResponse<ExchangeRates>
}