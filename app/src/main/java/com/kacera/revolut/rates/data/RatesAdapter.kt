package com.kacera.revolut.rates.data

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.VisibleForTesting
import androidx.collection.LruCache
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kacera.revolut.rates.R
import com.kacera.revolut.rates.data.model.CurrencyRateItem
import com.kacera.revolut.rates.databinding.LayoutCurrencyRateItemBinding
import com.kacera.revolut.rates.utils.currencyNames
import java.text.NumberFormat
import java.util.*

private val flagCache: LruCache<String, Drawable> = LruCache(40)

private fun resolveFlagId(currency: String, context: Context) =
    context.resources.getIdentifier(
        "ic_flag_${currency.toLowerCase(Locale.ROOT)}",
        "drawable",
        context.packageName
    )

private fun getFlagDrawable(currency: String, context: Context): Drawable? {
    return flagCache.get(currency) ?: ContextCompat.getDrawable(context,
        resolveFlagId(currency, context).let { id ->
            if (id != 0) id else R.drawable.ic_flag_unknown
        })
        .also {
            it?.let { flagCache.put(currency, it) }
        }
}

class RatesAdapter(
    private val formatter: NumberFormat,
    private val clickListener: (CurrencyRateItem) -> Unit,
    private val textWatcher: TextWatcher
) :
    RecyclerView.Adapter<RatesAdapter.RateItemViewHolder>() {

    private val items: MutableList<CurrencyRateItem> = mutableListOf()

    init {
        setHasStableIds(true)
    }

    fun setItems(items: List<CurrencyRateItem>) {
        DiffUtil.calculateDiff(DiffCallback(this.items, items)).also {
            it.dispatchUpdatesTo(this)
        }

        this.items.apply {
            clear()
            addAll(items.map { it.copy() })
        }
    }

    //mainly for testing purposes
    @VisibleForTesting
    fun getItem(position: Int) = items[position]

    override fun getItemId(position: Int): Long {
        return items[position].currency.hashCode().toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = RateItemViewHolder(
        LayoutCurrencyRateItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: RateItemViewHolder, position: Int) {
        holder.bind(position, items[position])
    }

    override fun onBindViewHolder(
        holder: RateItemViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isNotEmpty()) {
            payloads.forEach {
                when (it) {
                    DiffCallback.AMOUNT_CHANGE_PAYLOAD -> holder.bindEditText(
                        position,
                        items[position].amount
                    )
                    DiffCallback.BASE_CURRENCY_CHANGE_PAYLOAD -> holder.bindEditText(position)
                }
            }
        } else {
            super.onBindViewHolder(holder, position, payloads)
        }
    }

    override fun getItemCount() = items.size

    inner class RateItemViewHolder(
        private val binding: LayoutCurrencyRateItemBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {

        private var amount: String
            get() = binding.amount.text.toString()
            set(value) = binding.amount.setText(value, TextView.BufferType.EDITABLE)

        init {
            binding.root.setOnClickListener { clickListener(items[adapterPosition]) }
            binding.clickThief.setOnClickListener { clickListener(items[adapterPosition]) }
        }

        fun bind(position: Int, item: CurrencyRateItem) {
            binding.currency.text = item.currency
            binding.flag.setImageDrawable(getFlagDrawable(item.currency, binding.root.context))
            binding.description.text = currencyNames[item.currency] ?: "N/A"

            bindEditText(position, item.amount)
        }

        fun bindEditText(position: Int, amount: Double? = null) {
            val selectionStart = binding.amount.selectionStart
            val selectionEnd = binding.amount.selectionEnd

            binding.amount.removeTextChangedListener(textWatcher)

            if (amount != null && (position != 0 || amount != 0.0)) {
                this.amount = formatter.format(amount)
            }

            binding.clickThief.isClickable = position != 0

            if (position == 0) {
                binding.amount.requestFocus()
                binding.amount.setSelection(
                    if (selectionStart == 0) this.amount.length else selectionStart.coerceAtMost(this.amount.length),
                    if (selectionEnd == 0) this.amount.length else selectionEnd.coerceAtMost(this.amount.length)
                )
                binding.amount.addTextChangedListener(textWatcher)
            }
        }
    }

    private class DiffCallback(
        private val oldList: List<CurrencyRateItem>,
        private val newList: List<CurrencyRateItem>
    ) : DiffUtil.Callback() {

        companion object {
            internal const val AMOUNT_CHANGE_PAYLOAD = 1
            internal const val BASE_CURRENCY_CHANGE_PAYLOAD = 2
        }

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].currency == newList[newItemPosition].currency

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]
                    && !((oldItemPosition == 0 && newItemPosition != 0)
                    || (newItemPosition == 0 && oldItemPosition != 0))

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
            return when {
                oldList[oldItemPosition].amount != newList[newItemPosition].amount -> AMOUNT_CHANGE_PAYLOAD
                oldItemPosition == 0 || newItemPosition == 0 -> BASE_CURRENCY_CHANGE_PAYLOAD
                else -> super.getChangePayload(oldItemPosition, newItemPosition)
            }
        }
    }
}