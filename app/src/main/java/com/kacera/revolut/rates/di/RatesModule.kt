package com.kacera.revolut.rates.di

import com.kacera.revolut.rates.data.RatesRepository
import com.kacera.revolut.rates.data.RatesRepositoryImpl
import com.kacera.revolut.rates.network.ExchangeRatesApi
import com.kacera.revolut.rates.timer.ReloadTimer
import com.kacera.revolut.rates.timer.TaskTimer
import com.kacera.revolut.rates.utils.ExchangeRatesJsonAdapter
import com.kacera.revolut.rates.viewmodel.RatesFragmentViewModel
import com.squareup.moshi.Moshi
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.text.NumberFormat

val appModule = module {

    viewModel { RatesFragmentViewModel(get(), get(), get()) }

    single<NumberFormat> {
        NumberFormat.getNumberInstance().apply {
            maximumFractionDigits = 4
        }
    }

    single<ReloadTimer> { TaskTimer() }

    single<RatesRepository> { RatesRepositoryImpl(get()) }

    single {
        Retrofit.Builder()
            .baseUrl(ExchangeRatesApi.BASE_URL)
            .addConverterFactory(
                MoshiConverterFactory.create(
                    Moshi.Builder()
                        .add(ExchangeRatesJsonAdapter())
                        .build()
                )
            )
            .build()
            .create(ExchangeRatesApi::class.java)
    }
}