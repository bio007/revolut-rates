package com.kacera.revolut.rates.timer

import androidx.lifecycle.LiveData
import java.util.*

private const val TIMER_INTERVAL_IN_MS = 1000L

class TaskTimer : ReloadTimer {

    private val timer: Timer by lazy { Timer() }

    override val reload: LiveData<Unit> = object : LiveData<Unit>() {

        private lateinit var task: TimerTask

        override fun onActive() {
            task = object : TimerTask() {
                override fun run() {
                    postValue(null)
                }
            }

            timer.schedule(task, 0, TIMER_INTERVAL_IN_MS)
        }

        override fun onInactive() {
            task.cancel()
        }
    }
}