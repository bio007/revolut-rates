package com.kacera.revolut.rates.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.kacera.revolut.rates.databinding.FragmentRatesBinding
import com.kacera.revolut.rates.viewmodel.RatesFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class RatesFragment: Fragment() {

    private val fragmentViewModel by viewModel<RatesFragmentViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycle.addObserver(fragmentViewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentRatesBinding.inflate(inflater, container, false).apply {
            viewModel = fragmentViewModel
            lifecycleOwner = viewLifecycleOwner
        }.root
    }
}