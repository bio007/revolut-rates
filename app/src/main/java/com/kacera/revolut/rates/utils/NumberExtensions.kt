package com.kacera.revolut.rates.utils

import java.text.NumberFormat

fun NumberFormat.parseSafe(source: String): Number = if (source.length == 1 && source[0] == '.') {
    0
} else {
    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    parse(source)
}