package com.kacera.revolut.rates.timer

import androidx.lifecycle.LiveData

interface ReloadTimer {
    val reload: LiveData<Unit>
}