package com.kacera.revolut.rates.utils

import android.text.Editable
import android.text.TextWatcher

class TextChangedTextWatcher(val onChangeListener: (String) -> Unit): TextWatcher {

    override fun afterTextChanged(s: Editable) {
        //not interested in this call
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        //not interested in this call
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        onChangeListener(s.toString())
    }
}