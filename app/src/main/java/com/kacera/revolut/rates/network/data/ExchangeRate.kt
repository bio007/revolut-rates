package com.kacera.revolut.rates.network.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ExchangeRate(val currencyName: String, val conversionRate: Double)