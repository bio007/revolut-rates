package com.kacera.revolut.rates.utils

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("scrollPosition")
fun scrollPosition(recyclerView: RecyclerView, position: Int) {
    recyclerView.scrollToPosition(position)
}