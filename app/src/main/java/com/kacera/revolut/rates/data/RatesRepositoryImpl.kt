package com.kacera.revolut.rates.data

import com.kacera.revolut.rates.network.ExchangeRatesApi
import com.kacera.revolut.rates.network.data.ApiResponse
import com.kacera.revolut.rates.network.data.ExchangeRates

class RatesRepositoryImpl(private val api: ExchangeRatesApi) : RatesRepository {
    override suspend fun getRates(base: String?, symbols: String?): ApiResponse<ExchangeRates> {
        return try {
            ApiResponse.Success(api.getRates(base, symbols))
        } catch (e: Throwable) {
            ApiResponse.Error(e)
        }
    }
}