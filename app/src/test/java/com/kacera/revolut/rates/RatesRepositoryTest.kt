package com.kacera.revolut.rates

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.kacera.revolut.rates.data.RatesRepositoryImpl
import com.kacera.revolut.rates.network.ExchangeRatesApi
import com.kacera.revolut.rates.network.data.ApiResponse
import com.kacera.revolut.rates.network.data.ExchangeRates
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.*

@ExperimentalCoroutinesApi
class RatesRepositoryTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var api: ExchangeRatesApi

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `Api returns error`()  = testDispatcher.runBlockingTest  {
        api = mock {
            onBlocking {getRates(anyOrNull(), anyOrNull())} doThrow (RuntimeException())
        }

        with(RatesRepositoryImpl(api)) {
            Assert.assertThat(getRates(), instanceOf(ApiResponse.Error::class.java))
        }
    }

    @Test
    fun `Api returns success`()  = testDispatcher.runBlockingTest  {
        api = mock {
            onBlocking {getRates(anyOrNull(), anyOrNull())} doReturn (ExchangeRates("EUR", listOf()))
        }

        with(RatesRepositoryImpl(api)) {
            Assert.assertThat(getRates(), instanceOf(ApiResponse.Success::class.java))
        }
    }
}