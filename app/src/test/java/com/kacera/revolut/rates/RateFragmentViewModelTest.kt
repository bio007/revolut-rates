package com.kacera.revolut.rates

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.kacera.revolut.rates.data.RatesRepository
import com.kacera.revolut.rates.data.model.CurrencyRateItem
import com.kacera.revolut.rates.network.data.ApiResponse
import com.kacera.revolut.rates.network.data.ExchangeRate
import com.kacera.revolut.rates.network.data.ExchangeRates
import com.kacera.revolut.rates.utils.TestLifecycleOwner
import com.kacera.revolut.rates.utils.TestTimer
import com.kacera.revolut.rates.viewmodel.RatesFragmentViewModel
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.stopKoin
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import java.text.NumberFormat
import java.text.ParseException


@ExperimentalCoroutinesApi
class RateFragmentViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private val testDispatcher = TestCoroutineDispatcher()

    private var timer: TestTimer =
        TestTimer()

    private val formatter: NumberFormat = NumberFormat.getNumberInstance()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        stopKoin()
    }

    @Test
    fun `Remote API called on every timer's refresh`() = testDispatcher.runBlockingTest {
        withSuccessRepository { repository ->
            val viewModelTest = RatesFragmentViewModel(repository, timer, formatter)
            viewModelTest.onCreate(TestLifecycleOwner())

            verifyBlocking(repository, never()) { repository.getRates(anyOrNull(), anyOrNull()) }
            timer.emitReload()
            timer.emitReload()
            verifyBlocking(repository, times(2)) { repository.getRates(anyOrNull(), anyOrNull()) }
        }
    }

    @Test
    fun `Remote API call ends with error`() = testDispatcher.runBlockingTest {
        withFailingRepository { repository ->
            val viewModelTest = RatesFragmentViewModel(repository, timer, formatter)
            viewModelTest.onCreate(TestLifecycleOwner())

            assertEquals(0, viewModelTest.adapter.itemCount)

            timer.emitReload()
            runBlocking(testDispatcher) {
                assertEquals(0, viewModelTest.adapter.itemCount)
                assertTrue(viewModelTest.apiError.value!!)
            }
        }
    }

    @Test
    fun `ViewModel loads remote items`() = testDispatcher.runBlockingTest {
        val exchangeRates = ExchangeRates(
            "GBP",
            listOf(
                ExchangeRate("EUR", 1.1187),
                ExchangeRate("SEK", 11.848),
                ExchangeRate("PHP", 70.021)
            )
        )

        withSuccessRepository(exchangeRates) { repository ->
            val viewModelTest = RatesFragmentViewModel(repository, timer, formatter)
            viewModelTest.onCreate(TestLifecycleOwner())

            assertEquals(0, viewModelTest.adapter.itemCount)

            timer.emitReload()
            runBlocking(testDispatcher) {
                assertEquals(4, viewModelTest.adapter.itemCount)
                assertFalse(viewModelTest.apiError.value!!)

                assertEquals(CurrencyRateItem("GBP"), viewModelTest.adapter.getItem(0))
                assertEquals(CurrencyRateItem("EUR", 1.1187), viewModelTest.adapter.getItem(1))
                assertEquals(CurrencyRateItem("SEK", 11.848), viewModelTest.adapter.getItem(2))
                assertEquals(CurrencyRateItem("PHP", 70.021), viewModelTest.adapter.getItem(3))
            }
        }
    }

    @Test(expected = ParseException::class)
    fun `ViewModel input non-numeric value`() = testDispatcher.runBlockingTest {
        withSuccessRepository { repository ->
            val viewModelTest = RatesFragmentViewModel(repository, timer, formatter)
            viewModelTest.onCreate(TestLifecycleOwner())

            timer.emitReload()
            runBlocking(testDispatcher) {
                viewModelTest.onInputAmount("wrong")
            }
        }
    }

    @Test
    fun `ViewModel computes correct amount`() = testDispatcher.runBlockingTest {
        val input = 43.56
        val exchangeRates = ExchangeRates(
            "GBP",
            listOf(
                ExchangeRate("EUR", 1.1187),
                ExchangeRate("SEK", 11.848),
                ExchangeRate("PHP", 70.021)
            )
        )

        withSuccessRepository(exchangeRates) { repository ->
            val viewModelTest = RatesFragmentViewModel(repository, timer, formatter)
            viewModelTest.onCreate(TestLifecycleOwner())

            timer.emitReload()
            runBlocking(testDispatcher) {
                assertEquals(4, viewModelTest.adapter.itemCount)

                viewModelTest.onInputAmount("$input")

                assertEquals(CurrencyRateItem("GBP", 1 * input), viewModelTest.adapter.getItem(0))
                assertEquals(CurrencyRateItem("EUR", 1.1187 * input), viewModelTest.adapter.getItem(1))
                assertEquals(CurrencyRateItem("SEK", 11.848 * input), viewModelTest.adapter.getItem(2))
                assertEquals(CurrencyRateItem("PHP", 70.021 * input), viewModelTest.adapter.getItem(3))
            }
        }
    }

    @Test
    fun `Selected item becomes first`() = testDispatcher.runBlockingTest {
        val exchangeRates = ExchangeRates(
            "GBP",
            listOf(
                ExchangeRate("EUR", 1.1187),
                ExchangeRate("SEK", 11.848),
                ExchangeRate("PHP", 70.021)
            )
        )

        withSuccessRepository(exchangeRates) { repository ->
            val viewModelTest = RatesFragmentViewModel(repository, timer, formatter)
            viewModelTest.onCreate(TestLifecycleOwner())

            timer.emitReload()
            runBlocking(testDispatcher) {
                assertEquals(4, viewModelTest.adapter.itemCount)

                assertEquals("GBP", viewModelTest.adapter.getItem(0).currency)
                assertEquals(1.0, viewModelTest.adapter.getItem(0).amount, 0.0)

                viewModelTest.onCurrencySelected(viewModelTest.adapter.getItem(3))

                assertEquals("PHP", viewModelTest.adapter.getItem(0).currency)
                assertEquals(70.021, viewModelTest.adapter.getItem(0).amount, 0.0)
                assertEquals("GBP", viewModelTest.adapter.getItem(1).currency)
                assertEquals(1.0, viewModelTest.adapter.getItem(1).amount, 0.0)
            }
        }
    }

    private inline fun withSuccessRepository(
        exchangeRates: ExchangeRates = ExchangeRates("EUR", listOf()),
        block: (RatesRepository) -> Unit
    ) {
        block(
            mock {
                onBlocking { getRates(anyOrNull(), anyOrNull()) } doReturn ApiResponse.Success(
                    exchangeRates
                )
            }
        )
    }

    private fun withFailingRepository(
        block: (RatesRepository) -> Unit
    ) {
        block(
            mock {
                onBlocking { getRates(anyOrNull(), anyOrNull()) } doReturn ApiResponse.Error(
                    RuntimeException()
                )
            }
        )
    }
}
