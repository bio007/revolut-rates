package com.kacera.revolut.rates.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kacera.revolut.rates.timer.ReloadTimer

class TestTimer : ReloadTimer {

    override val reload: LiveData<Unit> = MutableLiveData<Unit>()

    fun emitReload() {
        reload.asMutable().value = null
    }
}